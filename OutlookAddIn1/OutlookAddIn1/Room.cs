﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookAddIn1
{
    public enum Site
    {
        BEER_SHEVA,HERZLIA,HAIFA,GLILYAM
    };

    public struct Availability
    {
        public String availabiltyString;
        public DateTime startTime;
        public const int slotLength = 30;
    }

    public class FilterSpec
    {
        public double slotNumFromDuration;
        public TimeSlot timeSlot = new TimeSlot();
        public bool vcRequired;
        public bool projectorReqiured;
        public Dictionary<Site, int> siteToParticipants = new Dictionary<Site, int>();

        public override string ToString()
        {
            String res;
            res = this.slotNumFromDuration.ToString() + this.timeSlot.startTime.ToString() +
                this.timeSlot.endTime.ToString() + this.vcRequired.ToString() + this.projectorReqiured.ToString() + this.siteToParticipants.ToString();
            return res;
        }
    }

    public class TimeSlot
    {
        public DateTime startTime;
        public DateTime endTime;

        public override bool Equals(System.Object obj)
        {
        // If parameter is null return false.
        if (obj == null)
        {
            return false;
        }

        // If parameter cannot be cast to Point return false.
        TimeSlot s = obj as TimeSlot;
        if ((System.Object)s == null)
        {
            return false;
        }

        // Return true if the fields match:
        return startTime == s.startTime && endTime == s.endTime;
        }

        public override int GetHashCode()
        {
            return 0;
        }

         public String ToString(){
            String output = "from=" +startTime.ToString("f") + " to=" + endTime.ToString("f");
            return output;
        }
    }

    public class Room
    {
        public Room(String name,String email,Site site, int capacity, bool hasVC,bool hasProjector,bool hasPolycom)
        {
            this.name = name;
            this.emailAdress = email;
            this.site = site;
            this.capacity = capacity;
            this.hasVC = hasVC;
            this.hasProjector = hasProjector;
            this.hasPolycom = hasPolycom;
            mut = new Mutex();
            //GetFreeBusyData();
        }

        public Availability GetAvailability()
        {
            Availability tmp;
            mut.WaitOne();
            tmp = this.availability;
            mut.ReleaseMutex();

            return tmp;
        }

        public void GetFreeBusyData()
        {
            Outlook.AddressEntry addrEntry = 
                Globals.ThisAddIn.Application.Session.CreateRecipient(this.emailAdress).AddressEntry;

            mut.WaitOne();

            this.availability.startTime = DateTime.Now;
            this.availability.availabiltyString = addrEntry.GetFreeBusy(this.availability.startTime, Availability.slotLength, true);

            mut.ReleaseMutex();

        }

        public List<TimeSlot> CheckAvailableSlots(List<TimeSlot> requireTimeSlots, int durationInHalfHours)
        {
            List<TimeSlot> res = new List<TimeSlot>();
            
            foreach (TimeSlot timeSlot in requireTimeSlots)
            { 
                DateTime start = timeSlot.startTime;
                DateTime end = timeSlot.endTime;

                int availableSlotDuration = 0;
                DateTime availableSlotStartTime = DateTime.Now;
                Availability tmpAvailability = GetAvailability();
                for (int i = 0; i < tmpAvailability.availabiltyString.Length; i++)
                {
                    if (tmpAvailability.availabiltyString.Substring(i, 1) == "0")
                    {
                        // Get number of minutes into
                        // the day for free interval
                        double busySlot = i * Availability.slotLength;
                        // Get an actual date/time
                        DateTime dateBusySlot =
                             tmpAvailability.startTime.Date.AddMinutes(busySlot);
                        if (dateBusySlot >= start & dateBusySlot < end)
                        {
                            if (availableSlotDuration == 0)
                            {
                                availableSlotStartTime = dateBusySlot;
                            }

                            availableSlotDuration++;
                            if (availableSlotDuration == durationInHalfHours)
                            {
                                TimeSlot t = new TimeSlot();
                                t.startTime = availableSlotStartTime;
                                t.endTime = dateBusySlot.AddMinutes(Availability.slotLength);
                                res.Add(t);
                                //reset for next available slot
                                availableSlotStartTime = availableSlotStartTime.AddMinutes(Availability.slotLength);
                                availableSlotDuration--;
                            }
                        }
                    }
                    else
                    {
                        availableSlotDuration = 0;
                    }
                }
            }

            return res;
        }

        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to Point return false.
            Room s = obj as Room;
            if ((System.Object)s == null)
            {
                return false;
            }

            // Return true if the fields match:
            return emailAdress == s.emailAdress;
        }

        public override int GetHashCode()
        {
            return 0;
        }


        public String emailAdress;
        public String name;
        public Site site;
        public int capacity;
        public bool hasVC;
        public bool hasPolycom;
        public bool hasProjector;
        private Availability availability;
        private Mutex mut;
    }
}
