﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;

namespace OutlookAddIn1
{
    public class Meeting
    {
        public TimeSlot meetingTime;
        public Dictionary<Site, HashSet<Room>> rooms = new Dictionary<Site, HashSet<Room>>();

        public String ToString()
        {
            String output = meetingTime.ToString();

            foreach (KeyValuePair<Site, HashSet<Room>> kvp in rooms)
            {
                output += "Start Site\n site=" + kvp.Key.ToString();
                foreach (Room room in kvp.Value)
                {
                    output += "room= " + room.name;
                }
                output += "\nEnd site";
            }
            return output;
        }


    }

    static class StaticRoomsList
    {
        static public List<Room> rooms = new List<Room>()
        {
            // Haifa
            new Room("Fl 3 Hermon - N2025","CRIsraelHaifaFI3Hermon@emc.com", Site.HAIFA, 8, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("Fl 3 Peleg - N2031", "CR-IsraelHaifaFI3Pel@emc.com", Site.HAIFA, 14, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("Fl 3 Arbel - N2048","CR-IsraelHaifaFI3Arb@emc.com", Site.HAIFA, 4, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("Fl 3 Tavor - N2061","CR-IsraelHaifaFl3Tavor-N206112s@emc.com", Site.HAIFA, 12, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("Fl 3 Yarden - S219A","CR-IsraelHAIFA3rdfloor-Yarden10s@emc.com",Site.HAIFA, 10, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("Fl 3 Galim - S218","CR-IsraelHAIFAFl3Oren-S2538s@emc.com",Site.HAIFA, 4, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("Fl 3 Barak  - S219","CR-IsraelHAIFA3rdfloor-Barak10s@emc.com",Site.HAIFA, 10, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("Fl 3 Lotem - S220A","CR-IsraelHaifaFI3LotemHDVideo@emc.com",Site.HAIFA, 11, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("Fl 3 Alon - S220 (10s) VC","CR-IsraelHaifaFI3Alon@emc.com",Site.HAIFA, 10, hasVC:true, hasProjector:false, hasPolycom:false),
            new Room("Fl 3 Oren - S253","CR-IsraelHAIFAFl3Oren-S2538s@emc.com",Site.HAIFA, 8, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("Fl 3 Carmel - S261 (14s) - VC","CR-IsraelHAIFAFl3CarmelHDVideo@emc.com",Site.HAIFA, 14, hasVC:true, hasProjector:true, hasPolycom:false),
            new Room("Fl3 Golan","svc_cresrl2haifa@emc.com",Site.HAIFA, 0, hasVC:false, hasProjector:false, hasPolycom:false),
            // Beer-Sheva
            new Room("Fl1 Negev HDVideo", "CR-IsraelBEERSHEVAFl1.NegevHDVideo@emc.com", Site.BEER_SHEVA, 15, hasVC:true, hasProjector:false, hasPolycom:false),
            new Room("Fl1 Arava HDVideo", "CR-IsraelBEERSHEVAFl1.AravaHDVideo@emc.com", Site.BEER_SHEVA, 9, hasVC:true, hasProjector:true, hasPolycom:false),
            new Room("Fl1 Training Center Restricted", "CR-IsraelBEERSHEVAFl1.TrainingCenterRestricted@emc.com", Site.BEER_SHEVA, 70, hasVC:true, hasProjector:false, hasPolycom:false),
            new Room("Fl1 Ramon HDVideo", "CR-IsraelBEERSHEVAFl1.RamonHDVideo@emc.com", Site.BEER_SHEVA, 11, hasVC:true, hasProjector:true, hasPolycom:false),
            new Room("FI1 Mobile", "CR-IsraelBeer-ShevaF@emc.com", Site.BEER_SHEVA, 5, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("Fl1 Ben Gurion HDVideo", "CR-IsraelBEERSHEVAFl1BenGurionHDVideo@emc.com", Site.BEER_SHEVA, 11, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("Fl1 EinGedi Video", "CR-IsraelBeerShevaFl1Eingedivideo@emc.com", Site.BEER_SHEVA, 6, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("Fl1 Office 273 Restricted", "CRIsraelBeerShevaBEE@emc.com", Site.BEER_SHEVA, 4, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("Fl1 Sinai Video", "CR-IsraelBeerShevaFl1Sinaivideo@emc.com", Site.BEER_SHEVA, 6, hasVC:true, hasProjector:true, hasPolycom:true),
            // Glil-Yam
            new Room("F1 - Small", "CRIsraelXIO-F1-Small@emc.com", Site.GLILYAM, 9, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("F2 - Big", "CRIsraelXIO-F2-Big@emc.com", Site.GLILYAM, 14, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("F2 - Small", "CRIsraelXIO-F2-Small@emc.com", Site.GLILYAM, 10, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("Hod Ami", "CRIsraelHERZLIYAHodA@emc.com", Site.GLILYAM, 5, hasVC:true, hasProjector:true, hasPolycom:true),
            new Room("BN", "CRIsraelXIO-BN@emc.com", Site.GLILYAM, 6, hasVC:true, hasProjector:true, hasPolycom:true),
            //Herzeliya
            new Room("1st floor - E017", "CRIsrael1stfloorE017@emc.com", Site.HERZLIA, 10, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("1st floor - W017", "CRIsrael1stfloorW017@emc.com", Site.HERZLIA, 10, hasVC:true, hasProjector:false, hasPolycom:false),
            new Room("1st floor - W105", "CRIsrael1stfloorW105@emc.com", Site.HERZLIA, 16, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("1st Floor - N113 HDVideo", "CRIsrael1stFloorN113@emc.com", Site.HERZLIA, 8, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("1st Floor - N143", "CRIsrael1stFloorN143@emc.com", Site.HERZLIA, 13, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("1st Floor - N141", "CRIsrael1stFloorN141@emc.com", Site.HERZLIA, 4, hasVC:false, hasProjector:false, hasPolycom:false),
            new Room("2nd floor - E027", "CRIsrael2ndfloorE027@emc.com", Site.HERZLIA, 10, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("2nd floor - W027", "CRIsrael2ndfloorW027@emc.com", Site.HERZLIA, 10, hasVC:true, hasProjector:true, hasPolycom:false),
            new Room("2nd floor - W239", "CRIsrael2ndfloorW239@emc.com", Site.HERZLIA, 8, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("3rd floor - E037", "CRIsrael3rdfloorE037@emc.com", Site.HERZLIA, 10, hasVC:true, hasProjector:true, hasPolycom:false),
            new Room("3rd floor - E350", "CRIsrael3rdfloorE350@emc.com", Site.HERZLIA, 6, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("3rd floor - W037", "CRIsrael3rdfloorW037@emc.com", Site.HERZLIA, 10, hasVC:true, hasProjector:true, hasPolycom:false),
            new Room("3rd floor - W300", "CRIsrael3rdfloorW300@emc.com", Site.HERZLIA, 8, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("3rd floor - W301", "CRIsrael3rdfloorW301@emc.com", Site.HERZLIA, 14, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("4th floor - W410", "CRIsrael4thfloorW410@emc.com", Site.HERZLIA, 10, hasVC:false, hasProjector:true, hasPolycom:false),
            new Room("4th floor - W446", "CRIsrael4thfloorW446@emc.com", Site.HERZLIA, 10, hasVC:false, hasProjector:true, hasPolycom:false),
        };

        static public List<Room> FilterRooms(FilterSpec filterSpec)
        {
            List<Room> relevatRooms = new List<Room>();


            foreach (Room room in rooms)
            {
                if ((!filterSpec.projectorReqiured || (filterSpec.projectorReqiured && room.hasProjector)) &&
                  (!filterSpec.vcRequired || (filterSpec.vcRequired && room.hasVC)) &&
                  (filterSpec.siteToParticipants.ContainsKey(room.site)) &&
                  (filterSpec.siteToParticipants[room.site] <= room.capacity))
                {
                    relevatRooms.Add(room);
                }
            }
            return relevatRooms;
        }
    }
}

