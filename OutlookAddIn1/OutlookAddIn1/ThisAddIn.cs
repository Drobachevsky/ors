﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using System.ComponentModel;

namespace OutlookAddIn1
{
    public partial class ThisAddIn
    {
        public SearchForm searchForm;
        
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            //AsyncOperationManager.SynchronizationContext = new WindowsFormsSynchronizationContext();
            searchForm = new SearchForm();
        }

  

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
        }

        public void ss()
        {
            Outlook.AppointmentItem agendaMeeting = (Outlook.AppointmentItem)
           this.Application.CreateItem(Microsoft.Office.Interop.Outlook.OlItemType.olAppointmentItem);

            if (agendaMeeting != null)
            {
                agendaMeeting.MeetingStatus =
                    Microsoft.Office.Interop.Outlook.OlMeetingStatus.olMeeting;
                agendaMeeting.Location = "Conference Room";
                agendaMeeting.Subject = "Discussing the Agenda";
                agendaMeeting.Body = "Let's discuss the agenda.";
                agendaMeeting.Start = new DateTime(2019, 2, 6, 18, 0, 0);
                agendaMeeting.End = new DateTime(2019, 2, 6, 19, 0, 0);
                agendaMeeting.Duration = 60;
                Outlook.Recipient recipient =
                    agendaMeeting.Recipients.Add("CR-Israel BEER SHEVA Fl1 Negev HDVideo");
                recipient.Type =
                    (int)Outlook.OlMeetingRecipientType.olRequired;
                ((Outlook._AppointmentItem)agendaMeeting).Send();
            }
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
