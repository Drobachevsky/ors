﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace OutlookAddIn1
{
    public partial class SearchForm : Form
    {
        private bool _initialAvailabilityLoaded = false;
        private int _selectedRows = 0;
        private List<Meeting> _meetings;

        //duration in 30 minutes
        public List<Meeting> CheckAvailability(List<Room> candidates, List<TimeSlot> requestedSlots, int meetingDuration, Dictionary<Site, int> siteToParticipantsSpec)
        {
            List<Meeting> meetings = new List<Meeting>();

            Dictionary<Site, List<Room>> groupedRooms = GroupBySites(candidates);
            List<TimeSlot> possibleSlots = new List<TimeSlot>(requestedSlots);
            List<TimeSlot> currentOpenSlots = new List<TimeSlot>();
            Dictionary<TimeSlot, List<Room>> slotToRooms = new Dictionary<TimeSlot, List<Room>>();

            foreach (KeyValuePair<Site, List<Room>> group in groupedRooms)
            {
                currentOpenSlots.Clear();
                currentOpenSlots.AddRange(possibleSlots);
                possibleSlots.Clear();
                foreach (Room room in group.Value)
                {
                    List<TimeSlot> roomAvailableSlots = room.CheckAvailableSlots(currentOpenSlots, meetingDuration);

                    foreach (TimeSlot slot in roomAvailableSlots)
                    {
                        if (slotToRooms.ContainsKey(slot))
                        {
                            slotToRooms[slot].Add(room);
                        }
                        else
                        {
                            slotToRooms.Add(slot, new List<Room> { room });
                        }
                    }
                    possibleSlots.AddRange(roomAvailableSlots);
                }
            }


            Dictionary<TimeSlot, List<Room>> newDict = slotToRooms.Where(kvp => possibleSlots.Contains(kvp.Key)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

            foreach (KeyValuePair<TimeSlot, List<Room>> kvp in newDict)
            {
                Meeting meeting = new Meeting();
                meeting.meetingTime = kvp.Key;

                foreach (Room room in kvp.Value)
                {

                    if (meeting.rooms.ContainsKey(room.site))
                    {
                        meeting.rooms[room.site].Add(room);
                    }
                    else
                    {
                        meeting.rooms.Add(room.site, new HashSet<Room> {room});
                    }

                }

                bool addMeeting = true;
                foreach (KeyValuePair<Site, int> site in siteToParticipantsSpec)
                {
                    if (site.Value > 0 && !meeting.rooms.ContainsKey(site.Key)){
                        addMeeting = false;
                    }
                }
                if (addMeeting)
                {
                    meetings.Add(meeting);
                }
            }
            
            return meetings.OrderBy(meeting => meeting.meetingTime.startTime).ToList();
        }

        public Dictionary<Site, List<Room>> GroupBySites(List<Room> roomsToGroup)
        {
            Dictionary<Site, List<Room>> groupedRooms = new Dictionary<Site, List<Room>>();

            foreach (Room roomToGroup in roomsToGroup)
            {
                if (groupedRooms.ContainsKey(roomToGroup.site))
                {
                    groupedRooms[roomToGroup.site].Add(roomToGroup);
                }
                else
                {
                    groupedRooms.Add(roomToGroup.site, new List<Room> { roomToGroup });
                }
            }

            return groupedRooms;
        }

        public SearchForm()
        {
            InitializeComponent();
            this.label4.ForeColor = Color.Black;
            this.label4.Text = "Refresh in progress";
            this.searchButton.Enabled = false;            
        }

        private FilterSpec createFilterSpec()
        {
            FilterSpec filterSpec = new FilterSpec();

            if (herzeliaNumericUpDown.Value > 0)
            {
                filterSpec.siteToParticipants.Add(OutlookAddIn1.Site.HERZLIA, (int)herzeliaNumericUpDown.Value);
            }
            if (bsNumericUpDown.Value > 0)
            {
                filterSpec.siteToParticipants.Add(OutlookAddIn1.Site.BEER_SHEVA, (int)bsNumericUpDown.Value);
            }
            if (glilYamNumericUpDown.Value > 0)
            {
                filterSpec.siteToParticipants.Add(OutlookAddIn1.Site.GLILYAM, (int)glilYamNumericUpDown.Value);
            }
            if (bangaloreNumericUpDown.Value > 0)
            {
                filterSpec.siteToParticipants.Add(OutlookAddIn1.Site.HAIFA, (int)bangaloreNumericUpDown.Value);
            }

            // fill dates
            filterSpec.timeSlot.startTime = new DateTime(dateTimePicker1.Value.Date.Year, dateTimePicker1.Value.Date.Month, dateTimePicker1.Value.Date.Day, dateTimePicker2.Value.Hour, dateTimePicker2.Value.Minute, 0);
            filterSpec.timeSlot.endTime = new DateTime(dateTimePicker1.Value.Date.Year, dateTimePicker1.Value.Date.Month, dateTimePicker1.Value.Date.Day, dateTimePicker3.Value.Hour, dateTimePicker3.Value.Minute, 0);

            int durationIndex = comboBox1.SelectedIndex;
            filterSpec.slotNumFromDuration = 0.5 + 0.5 * durationIndex;

            // fill features
            filterSpec.projectorReqiured = false;
            filterSpec.vcRequired = false;
            foreach (int indexChecked in checkedListBox1.CheckedIndices)
            {
                if (indexChecked == 0)
                {
                    filterSpec.projectorReqiured = true;
                }
                else if (indexChecked == 1)
                {
                    filterSpec.vcRequired = true;
                }
            }
            return filterSpec;

        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            searchButton.Enabled = false;
            button2.Enabled = false;
            button1.Enabled = false;
            dataGridView1.Rows.Clear();
            FilterSpec filterSpec = createFilterSpec();
            List<Room> rooms = StaticRoomsList.FilterRooms(filterSpec);
            int duration = (int)Math.Round(filterSpec.slotNumFromDuration / 0.5);
            List<Meeting> meetings = CheckAvailability(rooms, new List<TimeSlot> { filterSpec.timeSlot }, duration, filterSpec.siteToParticipants);
            _meetings = meetings;
            postResults(meetings);
            searchButton.Enabled = true;
        }

        private void postResults(List<Meeting> meetings)
        {
            foreach (Meeting meeting in meetings)
            {
                int num = dataGridView1.Rows.Add();
                ((DataGridViewTextBoxCell)dataGridView1.Rows[num].Cells[1]).Value = meeting.meetingTime.startTime.ToShortTimeString() + "-" + meeting.meetingTime.endTime.ToShortTimeString();
                foreach (KeyValuePair<Site, HashSet<Room>> kvp in meeting.rooms)
                {
                    Site site = kvp.Key;
                    int cellIndex = 0;
                    switch (site)
                    {
                        case OutlookAddIn1.Site.BEER_SHEVA:
                            cellIndex = 2;
                            break;
                        case OutlookAddIn1.Site.HERZLIA:
                            cellIndex = 3;
                            break;
                        case OutlookAddIn1.Site.HAIFA:
                            cellIndex = 4;
                            break;
                        case OutlookAddIn1.Site.GLILYAM:
                            cellIndex = 5;
                            break;
                    }
                    DataGridViewComboBoxCell siteComboBox = dataGridView1.Rows[num].Cells[cellIndex] as DataGridViewComboBoxCell;
                    foreach (Room room in kvp.Value)
                    {
                        siteComboBox.Items.Add(room.name);
                    }
                    siteComboBox.Value = siteComboBox.Items[0];
                }
            }
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("No available rooms found", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int slotLength = 30;

            if (comboBox1.SelectedItem != null)
            {
                int x = int.Parse(comboBox1.SelectedItem.ToString());
                int numSlots = x / slotLength;

            }
        }

        private void BookRooms(List<Room> rooms, TimeSlot timeSlot, bool book)
        {
            Outlook.Application application = new Outlook.Application();
            Outlook.AppointmentItem agendaMeeting = (Outlook.AppointmentItem)
                application.CreateItem(Outlook.OlItemType.olAppointmentItem);

            StringBuilder roomNames = new StringBuilder();
            foreach (Room room in rooms)
            {
                if (roomNames.Length > 0)
                {
                    roomNames.Append(", ");
                }
                roomNames.Append(room.name);
            }
            string subjectText = textBox1.Text;
            if (string.IsNullOrEmpty(subjectText))
            {
                subjectText = "ORS Scheduled Meeting";
            }

            if (agendaMeeting != null)
            {
                agendaMeeting.MeetingStatus = Outlook.OlMeetingStatus.olMeeting;
                agendaMeeting.Location = roomNames.ToString();
                agendaMeeting.ReminderSet = true;
                agendaMeeting.ReminderMinutesBeforeStart = 15;
                    agendaMeeting.Subject = subjectText;
                agendaMeeting.Body = "ORS brought to you by VSAN - Hackathon 2019";
                agendaMeeting.Start = timeSlot.startTime;
                agendaMeeting.End = timeSlot.endTime;
                foreach (Room room in rooms)
                {
                    Outlook.Recipient recipient = agendaMeeting.Recipients.Add(room.emailAdress);
                    recipient.Type = (int)Outlook.OlMeetingRecipientType.olRequired;
                }

                if (book)
                {
                    agendaMeeting.Send();
                }
                else
                {
                    agendaMeeting.Recipients.ResolveAll();
                    agendaMeeting.Display(true);
                }
                    
            }
            //MessageBox.Show("Meeting scheduled successfully:\n " +
            //    "Rooms: " + roomNames.ToString() + "\n " +
            //    "Time: " + timeSlot.ToString());
        }

        private void ExecuteUserCommand(bool book)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataGridViewCheckBoxCell chkchecking = row.Cells[0] as DataGridViewCheckBoxCell;
                if (Convert.ToBoolean(chkchecking.Value) == true)
                {
                    List<Room> roomsToBook = new List<Room>();
                    Meeting meeting = _meetings[row.Index];
                    DataGridViewComboBoxCell beerShevaComboBox = row.Cells[2] as DataGridViewComboBoxCell;
                    if (meeting.rooms.ContainsKey(OutlookAddIn1.Site.BEER_SHEVA))
                    {
                        string beerShevaRoomName = beerShevaComboBox.Value.ToString();
                        foreach (Room room in meeting.rooms[OutlookAddIn1.Site.BEER_SHEVA])
                        {
                            if (room.name == beerShevaRoomName)
                            {
                                roomsToBook.Add(room);
                                break;
                            }
                        }
                    }
                    DataGridViewComboBoxCell herzliyaComboBox = row.Cells[3] as DataGridViewComboBoxCell;
                    if (meeting.rooms.ContainsKey(OutlookAddIn1.Site.HERZLIA))
                    {
                        string herzliyaRoomName = herzliyaComboBox.Value.ToString();
                        foreach (Room room in meeting.rooms[OutlookAddIn1.Site.HERZLIA])
                        {
                            if (room.name == herzliyaRoomName)
                            {
                                roomsToBook.Add(room);
                                break;
                            }
                        }
                    }
                    DataGridViewComboBoxCell haifaComboBox = row.Cells[4] as DataGridViewComboBoxCell;
                    if (meeting.rooms.ContainsKey(OutlookAddIn1.Site.HAIFA))
                    {
                        string haifaRoomName = haifaComboBox.Value.ToString();
                        foreach (Room room in meeting.rooms[OutlookAddIn1.Site.HAIFA])
                        {
                            if (room.name == haifaRoomName)
                            {
                                roomsToBook.Add(room);
                                break;
                            }
                        }
                    }
                    DataGridViewComboBoxCell glilYamComboBox = row.Cells[5] as DataGridViewComboBoxCell;
                    if (meeting.rooms.ContainsKey(OutlookAddIn1.Site.GLILYAM))
                    {
                        string glilYamRoomName = glilYamComboBox.Value.ToString();
                        foreach (Room room in meeting.rooms[OutlookAddIn1.Site.GLILYAM])
                        {
                            if (room.name == glilYamRoomName)
                            {
                                roomsToBook.Add(room);
                                break;
                            }
                        }
                    }
                    BookRooms(roomsToBook, meeting.meetingTime, book);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ExecuteUserCommand(true);
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewCheckBoxCell checkBox = dataGridView1.Rows[e.RowIndex].Cells[0] as DataGridViewCheckBoxCell;
            if (Convert.ToBoolean(checkBox.EditedFormattedValue))
            {
                dataGridView1.Rows[e.RowIndex].Cells[1].Style.BackColor = Color.Azure;
                ++_selectedRows;
            }
            else
            {
                dataGridView1.Rows[e.RowIndex].Cells[1].Style.BackColor = Color.White;
                --_selectedRows;
            }

            button1.Enabled = _selectedRows > 0 ? true : false;
            button2.Enabled = _selectedRows > 0 ? true : false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExecuteUserCommand(false);
            this.Hide();
        }

        private DateTime _prevDate1;
        private DateTime _prevDate2;

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = dateTimePicker2.Value;
            TimeSpan diff = dt - _prevDate1;

            if (diff.Ticks < 0)
                dateTimePicker2.Value = _prevDate1.Subtract(new TimeSpan(0, 30, 0));
            else
                dateTimePicker2.Value = _prevDate1.AddMinutes(30);

            _prevDate1 = dateTimePicker2.Value;
        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = dateTimePicker3.Value;
            TimeSpan diff = dt - _prevDate2;

            if (diff.Ticks < 0)
                dateTimePicker3.Value = _prevDate2.Subtract(new TimeSpan(0,30,0));
            else
                dateTimePicker3.Value = _prevDate2.AddMinutes(30);

            _prevDate2 = dateTimePicker3.Value;
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            InitFormValues();
            if (!_initialAvailabilityLoaded) {
                MessageBox.Show("We are loading rooms Free/Busy info. \nThe scheduler will be enabled soon.", "ORS info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            GetAvailabilityAsync();
        }

        private void dateTimePicker2_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void dateTimePicker3_KeyDown(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        private void GetAvailability(object sender, DoWorkEventArgs e)
        {
            bool res = true;
            try
            {
                foreach (Room room in StaticRoomsList.rooms)
                {
                    room.GetFreeBusyData();
                }
            }
            catch(Exception ex)
            {
                res = false;
            }

            e.Result = res;
        }
        
        private void GetAvailabilityCb(object sender, RunWorkerCompletedEventArgs e)
        {
            bool errExist = e.Error != null || !((bool)e.Result);
            avQueryInfo.ReportDone(errExist);
            if (!errExist)
            {
                this._initialAvailabilityLoaded = true;
            }
        }

        private void SearchForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                InitFormValues();
                Hide();
            }
        }
        
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.Enabled = false;
            InitFormValues();
            linkLabel1.Enabled = true;
        }

        private void InitFormValues()
        {
            this.button1.Enabled = false;
            this.button2.Enabled = false;
            this.dataGridView1.Rows.Clear();

            this.textBox1.Text = "ORS Scheduled Meeting";

            this.herzeliaNumericUpDown.Value = 0;
            this.bsNumericUpDown.Value = 0;
            this.glilYamNumericUpDown.Value = 0;
            this.bangaloreNumericUpDown.Value = 0;

            this._selectedRows = 0;

            DateTime currentDT = DateTime.Now;
            this.dateTimePicker1.Value = currentDT;
            this.comboBox1.SelectedIndex = 0;


            for (int i = 0; i < checkedListBox1.Items.Count; i++)
            {
                checkedListBox1.SetItemChecked(i, false);
            }

            this._prevDate1 = dateTimePicker2.Value;
            this._prevDate2 = dateTimePicker3.Value;

            this.dateTimePicker2.Value = DateTime.Parse("06-Feb-19 10:00:00 +2:00");
            this.dateTimePicker3.Value = DateTime.Parse("06-Feb-19 17:00:00 +2:00");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {           
            this.label4.ForeColor = Color.Black;
            this.label4.Text = "Refresh in progress";
            GetAvailabilityAsync();
        }

        void GetAvailabilityAsync()
        {
            avQueryInfo.ReportStart();
            linkLabel2.Enabled = false;

            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += this.GetAvailability;
            bgWorker.RunWorkerCompleted += this.GetAvailabilityCb;

            bgWorker.RunWorkerAsync();
        }

        public class AvailabilityQueryInfo
        {        
            private DateTime lastCompleetionTime;
            private bool isInProgress;
            private int errExist;
            private Mutex mut;

            public void ReportDone(bool errorExist)
            {
                mut.WaitOne();
                isInProgress = false;
                errExist = errorExist ? 2 : 1;
                if (!errorExist)
                {
                    lastCompleetionTime = DateTime.Now;
                }
                mut.ReleaseMutex();
            }

            public void ReportStart()
            {
                mut.WaitOne();
                isInProgress = true;
                errExist = 0;
                mut.ReleaseMutex();
            }

            public AvailabilityQueryInfo()
            {
                isInProgress = false;
                errExist = 0;
                mut = new Mutex();
            }

            public bool GetInProgress()
            {
                bool tmp;
                mut.WaitOne();
                tmp = isInProgress;
                mut.ReleaseMutex();
                return tmp;
            }

            public DateTime GetLastCompleetionTime()
            {
                DateTime tmp;
                mut.WaitOne();
                tmp = lastCompleetionTime;
                mut.ReleaseMutex();
                return tmp;
            }

            public int GetErrorExist()
            {
                int tmp;
                mut.WaitOne();
                tmp = errExist;
                errExist = 0;
                mut.ReleaseMutex();
                return tmp;
            }
            
        }

        public AvailabilityQueryInfo avQueryInfo = new AvailabilityQueryInfo();

        private void timer1_Tick(object sender, EventArgs e)
        {
            bool inProg = avQueryInfo.GetInProgress();
            int errExist = avQueryInfo.GetErrorExist();
            linkLabel2.Enabled = !inProg;
            if (!inProg && errExist == 2)
            {
                label4.ForeColor = Color.Red;
                label4.Text = "Failed to get Free/Busy info";

                MessageBox.Show("Failed to get Free/Busy info, \nCheck network connectivity and retry.", "ORS Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!inProg && errExist == 1)
            {
                var x = avQueryInfo.GetLastCompleetionTime();
                label3.Text = x.ToShortDateString() + " " + x.ToShortTimeString();

                label4.Text = "";
                if (this._initialAvailabilityLoaded)
                {
                    this.searchButton.Enabled = true;
                }
            }

            //this.Refresh();           
        }

        private void SearchForm_Activated(object sender, EventArgs e)
        {
            timer1.Start();
        }
    }
}
