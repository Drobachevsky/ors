﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddIn1
{

    public enum Site
    {
        BEER_SHEVA, HERZLIA, HAIFA, GLIL_YAM
    }

    struct SitePair
    {
        public Site site;
        public int participantsNum;
    }


    class FeaturesInput
    {
        public bool VC;
        public bool polycom;
        public bool projector;
        public List<SitePair> sites;
    }
}
