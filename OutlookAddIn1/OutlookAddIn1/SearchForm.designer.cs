﻿namespace OutlookAddIn1
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            this.roomScedLable = new System.Windows.Forms.Label();
            this.locationBox = new System.Windows.Forms.GroupBox();
            this.glilYamLabel = new System.Windows.Forms.Label();
            this.bangaloreLabel = new System.Windows.Forms.Label();
            this.bsLabel = new System.Windows.Forms.Label();
            this.herzliyaLabel = new System.Windows.Forms.Label();
            this.bangaloreNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.glilYamNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.bsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.herzeliaNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.featuresBox = new System.Windows.Forms.GroupBox();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateLabel = new System.Windows.Forms.Label();
            this.ToLabel = new System.Windows.Forms.Label();
            this.fromLabel = new System.Windows.Forms.Label();
            this.timeSlotGroupBox = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.durationLabel = new System.Windows.Forms.Label();
            this.resultsBox = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.choose = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.timeSlot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.beersheva = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.herzlyia = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.haifa = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.glilYam = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.locationBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bangaloreNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glilYamNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.herzeliaNumericUpDown)).BeginInit();
            this.featuresBox.SuspendLayout();
            this.timeSlotGroupBox.SuspendLayout();
            this.resultsBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // roomScedLable
            // 
            this.roomScedLable.AutoSize = true;
            this.roomScedLable.Font = new System.Drawing.Font("Corbel", 22.2F, System.Drawing.FontStyle.Bold);
            this.roomScedLable.ForeColor = System.Drawing.SystemColors.Highlight;
            this.roomScedLable.Location = new System.Drawing.Point(375, 9);
            this.roomScedLable.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.roomScedLable.Name = "roomScedLable";
            this.roomScedLable.Size = new System.Drawing.Size(228, 37);
            this.roomScedLable.TabIndex = 0;
            this.roomScedLable.Text = "Room Scheduler";
            // 
            // locationBox
            // 
            this.locationBox.Controls.Add(this.glilYamLabel);
            this.locationBox.Controls.Add(this.bangaloreLabel);
            this.locationBox.Controls.Add(this.bsLabel);
            this.locationBox.Controls.Add(this.herzliyaLabel);
            this.locationBox.Controls.Add(this.bangaloreNumericUpDown);
            this.locationBox.Controls.Add(this.glilYamNumericUpDown);
            this.locationBox.Controls.Add(this.bsNumericUpDown);
            this.locationBox.Controls.Add(this.herzeliaNumericUpDown);
            this.locationBox.Font = new System.Drawing.Font("Corbel", 12.5F, System.Drawing.FontStyle.Bold);
            this.locationBox.Location = new System.Drawing.Point(19, 61);
            this.locationBox.Margin = new System.Windows.Forms.Padding(2);
            this.locationBox.Name = "locationBox";
            this.locationBox.Padding = new System.Windows.Forms.Padding(2);
            this.locationBox.Size = new System.Drawing.Size(251, 195);
            this.locationBox.TabIndex = 4;
            this.locationBox.TabStop = false;
            this.locationBox.Text = "Locations + participants:";
            // 
            // glilYamLabel
            // 
            this.glilYamLabel.AutoSize = true;
            this.glilYamLabel.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.glilYamLabel.Location = new System.Drawing.Point(34, 110);
            this.glilYamLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.glilYamLabel.Name = "glilYamLabel";
            this.glilYamLabel.Size = new System.Drawing.Size(58, 18);
            this.glilYamLabel.TabIndex = 15;
            this.glilYamLabel.Text = "Glil-Yam";
            // 
            // bangaloreLabel
            // 
            this.bangaloreLabel.AutoSize = true;
            this.bangaloreLabel.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.bangaloreLabel.Location = new System.Drawing.Point(34, 146);
            this.bangaloreLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bangaloreLabel.Name = "bangaloreLabel";
            this.bangaloreLabel.Size = new System.Drawing.Size(40, 18);
            this.bangaloreLabel.TabIndex = 14;
            this.bangaloreLabel.Text = "Haifa";
            // 
            // bsLabel
            // 
            this.bsLabel.AutoSize = true;
            this.bsLabel.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.bsLabel.Location = new System.Drawing.Point(34, 73);
            this.bsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.bsLabel.Name = "bsLabel";
            this.bsLabel.Size = new System.Drawing.Size(77, 18);
            this.bsLabel.TabIndex = 13;
            this.bsLabel.Text = "Beer-Sheva";
            // 
            // herzliyaLabel
            // 
            this.herzliyaLabel.AutoSize = true;
            this.herzliyaLabel.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.herzliyaLabel.Location = new System.Drawing.Point(34, 37);
            this.herzliyaLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.herzliyaLabel.Name = "herzliyaLabel";
            this.herzliyaLabel.Size = new System.Drawing.Size(57, 18);
            this.herzliyaLabel.TabIndex = 12;
            this.herzliyaLabel.Text = "Herzliya";
            // 
            // bangaloreNumericUpDown
            // 
            this.bangaloreNumericUpDown.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.bangaloreNumericUpDown.Location = new System.Drawing.Point(124, 146);
            this.bangaloreNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.bangaloreNumericUpDown.Name = "bangaloreNumericUpDown";
            this.bangaloreNumericUpDown.Size = new System.Drawing.Size(90, 25);
            this.bangaloreNumericUpDown.TabIndex = 11;
            // 
            // glilYamNumericUpDown
            // 
            this.glilYamNumericUpDown.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.glilYamNumericUpDown.Location = new System.Drawing.Point(124, 110);
            this.glilYamNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.glilYamNumericUpDown.Name = "glilYamNumericUpDown";
            this.glilYamNumericUpDown.Size = new System.Drawing.Size(90, 25);
            this.glilYamNumericUpDown.TabIndex = 10;
            // 
            // bsNumericUpDown
            // 
            this.bsNumericUpDown.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.bsNumericUpDown.Location = new System.Drawing.Point(124, 73);
            this.bsNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.bsNumericUpDown.Name = "bsNumericUpDown";
            this.bsNumericUpDown.Size = new System.Drawing.Size(90, 25);
            this.bsNumericUpDown.TabIndex = 9;
            // 
            // herzeliaNumericUpDown
            // 
            this.herzeliaNumericUpDown.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.herzeliaNumericUpDown.Location = new System.Drawing.Point(124, 37);
            this.herzeliaNumericUpDown.Margin = new System.Windows.Forms.Padding(2);
            this.herzeliaNumericUpDown.Name = "herzeliaNumericUpDown";
            this.herzeliaNumericUpDown.Size = new System.Drawing.Size(90, 25);
            this.herzeliaNumericUpDown.TabIndex = 8;
            // 
            // featuresBox
            // 
            this.featuresBox.Controls.Add(this.checkedListBox1);
            this.featuresBox.Font = new System.Drawing.Font("Corbel", 12.5F, System.Drawing.FontStyle.Bold);
            this.featuresBox.Location = new System.Drawing.Point(431, 61);
            this.featuresBox.Margin = new System.Windows.Forms.Padding(2);
            this.featuresBox.Name = "featuresBox";
            this.featuresBox.Padding = new System.Windows.Forms.Padding(2);
            this.featuresBox.Size = new System.Drawing.Size(124, 122);
            this.featuresBox.TabIndex = 5;
            this.featuresBox.TabStop = false;
            this.featuresBox.Text = "Features:";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBox1.Font = new System.Drawing.Font("Corbel", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "Projector",
            "VC"});
            this.checkedListBox1.Location = new System.Drawing.Point(11, 32);
            this.checkedListBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(101, 40);
            this.checkedListBox1.TabIndex = 1;
            this.checkedListBox1.ThreeDCheckBoxes = true;
            // 
            // searchButton
            // 
            this.searchButton.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.searchButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.searchButton.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold);
            this.searchButton.ForeColor = System.Drawing.SystemColors.Highlight;
            this.searchButton.Location = new System.Drawing.Point(431, 195);
            this.searchButton.Margin = new System.Windows.Forms.Padding(2);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(124, 61);
            this.searchButton.TabIndex = 7;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = false;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.dateTimePicker1.Location = new System.Drawing.Point(24, 57);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(213, 25);
            this.dateTimePicker1.TabIndex = 8;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CustomFormat = "HH:mm";
            this.dateTimePicker2.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(71, 98);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowUpDown = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(57, 25);
            this.dateTimePicker2.TabIndex = 9;
            this.dateTimePicker2.Value = new System.DateTime(2019, 2, 6, 10, 0, 0, 0);
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            this.dateTimePicker2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePicker2_KeyDown);
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.CustomFormat = "HH:mm";
            this.dateTimePicker3.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker3.Location = new System.Drawing.Point(180, 98);
            this.dateTimePicker3.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.ShowUpDown = true;
            this.dateTimePicker3.Size = new System.Drawing.Size(57, 25);
            this.dateTimePicker3.TabIndex = 10;
            this.dateTimePicker3.Value = new System.DateTime(2019, 2, 6, 17, 0, 0, 0);
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            this.dateTimePicker3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePicker3_KeyDown);
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Font = new System.Drawing.Font("Corbel", 11F, System.Drawing.FontStyle.Bold);
            this.dateLabel.Location = new System.Drawing.Point(21, 37);
            this.dateLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(44, 18);
            this.dateLabel.TabIndex = 11;
            this.dateLabel.Text = "Date:";
            // 
            // ToLabel
            // 
            this.ToLabel.AutoSize = true;
            this.ToLabel.Font = new System.Drawing.Font("Corbel", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToLabel.Location = new System.Drawing.Point(148, 98);
            this.ToLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ToLabel.Name = "ToLabel";
            this.ToLabel.Size = new System.Drawing.Size(28, 18);
            this.ToLabel.TabIndex = 12;
            this.ToLabel.Text = "To:";
            // 
            // fromLabel
            // 
            this.fromLabel.AutoSize = true;
            this.fromLabel.Font = new System.Drawing.Font("Corbel", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fromLabel.Location = new System.Drawing.Point(21, 98);
            this.fromLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.fromLabel.Name = "fromLabel";
            this.fromLabel.Size = new System.Drawing.Size(46, 18);
            this.fromLabel.TabIndex = 13;
            this.fromLabel.Text = "From:";
            // 
            // timeSlotGroupBox
            // 
            this.timeSlotGroupBox.Controls.Add(this.dateTimePicker1);
            this.timeSlotGroupBox.Controls.Add(this.comboBox1);
            this.timeSlotGroupBox.Controls.Add(this.durationLabel);
            this.timeSlotGroupBox.Controls.Add(this.fromLabel);
            this.timeSlotGroupBox.Controls.Add(this.dateTimePicker2);
            this.timeSlotGroupBox.Controls.Add(this.ToLabel);
            this.timeSlotGroupBox.Controls.Add(this.dateTimePicker3);
            this.timeSlotGroupBox.Controls.Add(this.dateLabel);
            this.timeSlotGroupBox.Font = new System.Drawing.Font("Corbel", 12.5F, System.Drawing.FontStyle.Bold);
            this.timeSlotGroupBox.Location = new System.Drawing.Point(712, 61);
            this.timeSlotGroupBox.Margin = new System.Windows.Forms.Padding(2);
            this.timeSlotGroupBox.Name = "timeSlotGroupBox";
            this.timeSlotGroupBox.Padding = new System.Windows.Forms.Padding(2);
            this.timeSlotGroupBox.Size = new System.Drawing.Size(251, 195);
            this.timeSlotGroupBox.TabIndex = 16;
            this.timeSlotGroupBox.TabStop = false;
            this.timeSlotGroupBox.Text = "Time window:";
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Corbel", 10.8F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "0.5",
            "1.0",
            "1.5",
            "2.0",
            "2.5",
            "3.0",
            "3.5",
            "4.0",
            "4.5",
            "5.0"});
            this.comboBox1.Location = new System.Drawing.Point(24, 157);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(125, 25);
            this.comboBox1.TabIndex = 17;
            this.comboBox1.Text = "0.5";
            // 
            // durationLabel
            // 
            this.durationLabel.AutoSize = true;
            this.durationLabel.Font = new System.Drawing.Font("Corbel", 11F, System.Drawing.FontStyle.Bold);
            this.durationLabel.Location = new System.Drawing.Point(20, 134);
            this.durationLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.durationLabel.Name = "durationLabel";
            this.durationLabel.Size = new System.Drawing.Size(69, 18);
            this.durationLabel.TabIndex = 18;
            this.durationLabel.Text = "Duration:";
            // 
            // resultsBox
            // 
            this.resultsBox.Controls.Add(this.dataGridView1);
            this.resultsBox.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Bold);
            this.resultsBox.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.resultsBox.Location = new System.Drawing.Point(22, 280);
            this.resultsBox.Margin = new System.Windows.Forms.Padding(2);
            this.resultsBox.Name = "resultsBox";
            this.resultsBox.Padding = new System.Windows.Forms.Padding(2);
            this.resultsBox.Size = new System.Drawing.Size(954, 309);
            this.resultsBox.TabIndex = 20;
            this.resultsBox.TabStop = false;
            this.resultsBox.Text = "Results";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.choose,
            this.timeSlot,
            this.beersheva,
            this.herzlyia,
            this.haifa,
            this.glilYam});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(2, 22);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(950, 285);
            this.dataGridView1.TabIndex = 20;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // choose
            // 
            this.choose.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.choose.HeaderText = "[]";
            this.choose.Name = "choose";
            this.choose.Width = 25;
            // 
            // timeSlot
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            this.timeSlot.DefaultCellStyle = dataGridViewCellStyle8;
            this.timeSlot.HeaderText = "Time Slot";
            this.timeSlot.Name = "timeSlot";
            this.timeSlot.Width = 150;
            // 
            // beersheva
            // 
            this.beersheva.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.White;
            this.beersheva.DefaultCellStyle = dataGridViewCellStyle9;
            this.beersheva.HeaderText = "Beer-Sheva";
            this.beersheva.Name = "beersheva";
            // 
            // herzlyia
            // 
            this.herzlyia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.herzlyia.HeaderText = "Herzlyia";
            this.herzlyia.Name = "herzlyia";
            // 
            // haifa
            // 
            this.haifa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.haifa.HeaderText = "Haifa";
            this.haifa.Name = "haifa";
            // 
            // glilYam
            // 
            this.glilYam.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.glilYam.HeaderText = "Glil-Yam";
            this.glilYam.Name = "glilYam";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::OutlookAddIn1.Properties.Resources.Dell_EMC_logo_svg;
            this.pictureBox1.Location = new System.Drawing.Point(19, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 29);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button2.Enabled = false;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Location = new System.Drawing.Point(118, 628);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(90, 45);
            this.button2.TabIndex = 8;
            this.button2.Text = "Book";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.button1.Enabled = false;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Candara", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(24, 628);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 45);
            this.button1.TabIndex = 22;
            this.button1.Text = "Edit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(98, 595);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(876, 20);
            this.textBox1.TabIndex = 23;
            this.textBox1.Text = "ORS Scheduled Meeting";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Corbel", 11F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(31, 595);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 18);
            this.label1.TabIndex = 19;
            this.label1.Text = "Subject:";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(906, 265);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(57, 13);
            this.linkLabel1.TabIndex = 25;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Reset filter";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(762, 628);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(126, 13);
            this.linkLabel2.TabIndex = 26;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Refresh rooms availability";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(762, 643);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Last refresh time:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(853, 643);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(762, 656);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "status";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(987, 683);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.resultsBox);
            this.Controls.Add(this.timeSlotGroupBox);
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.featuresBox);
            this.Controls.Add(this.locationBox);
            this.Controls.Add(this.roomScedLable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = global::OutlookAddIn1.Properties.Resources.ors_icon;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "SearchForm";
            this.Text = "Room Scheduler";
            this.Activated += new System.EventHandler(this.SearchForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SearchForm_FormClosing);
            this.Load += new System.EventHandler(this.SearchForm_Load);
            this.locationBox.ResumeLayout(false);
            this.locationBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bangaloreNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glilYamNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.herzeliaNumericUpDown)).EndInit();
            this.featuresBox.ResumeLayout(false);
            this.timeSlotGroupBox.ResumeLayout(false);
            this.timeSlotGroupBox.PerformLayout();
            this.resultsBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label roomScedLable;
        private System.Windows.Forms.GroupBox locationBox;
        private System.Windows.Forms.GroupBox featuresBox;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.Label glilYamLabel;
        private System.Windows.Forms.Label bangaloreLabel;
        private System.Windows.Forms.Label bsLabel;
        private System.Windows.Forms.Label herzliyaLabel;
        private System.Windows.Forms.NumericUpDown bangaloreNumericUpDown;
        private System.Windows.Forms.NumericUpDown glilYamNumericUpDown;
        private System.Windows.Forms.NumericUpDown bsNumericUpDown;
        private System.Windows.Forms.NumericUpDown herzeliaNumericUpDown;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label ToLabel;
        private System.Windows.Forms.Label fromLabel;
        private System.Windows.Forms.GroupBox timeSlotGroupBox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label durationLabel;
        private System.Windows.Forms.GroupBox resultsBox;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewCheckBoxColumn choose;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeSlot;
        private System.Windows.Forms.DataGridViewComboBoxColumn beersheva;
        private System.Windows.Forms.DataGridViewComboBoxColumn herzlyia;
        private System.Windows.Forms.DataGridViewComboBoxColumn haifa;
        private System.Windows.Forms.DataGridViewComboBoxColumn glilYam;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

